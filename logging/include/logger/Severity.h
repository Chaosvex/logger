/*
 * Copyright (c) 2015 Ember
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#undef ERROR //preventing a conflict with wingdi.h (dragged in indirectly by ASIO)

namespace ember { namespace log {

enum class SEVERITY { TRACE, DEBUG, INFO, WARN, ERROR, FATAL, DISABLED };

}} //log, ember